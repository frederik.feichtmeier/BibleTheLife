library bible.cache_tab_bo;

class CacheTabBO {
  int tabId;
  String tabType;
  String tabTitle;
  String fullQuery;
  int scrollPosY;
  String bbName;
  int isBook;
  int isChapter;
  int isVerse;
  int bNumber;
  int cNumber;
  int vNumber;
  String trad;
  int orderBy;
  int favFilter;
}
