import 'package:flutter/material.dart';

class BookTitle extends TextSpan {
  final TextSpan title;

  BookTitle(this.title);
}
